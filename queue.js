let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    let collLength = collection.length
    collection[collLength] = element;
    return collection
}

function dequeue(){
    // removes first element
    const newColl = [];
    for (let i = 1; i < collection.length; i++) {
      newColl[i - 1] = collection[i];
    }
    collection = newColl;
    return collection;
}

function front() {
    // return first element of array
    let firstElem = collection[0];
    return firstElem;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements
    let i = 0
    while(collection[i]){
        i++
    }
    return i

}

function isEmpty() {
    //it will check whether the function is empty or not
    if(!collection) 
       { return true; }
    return false
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};